﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;

namespace API.Helpers
{
    /// <summary>
    /// Returns the driver instance depending of the configuration value Browser
    /// </summary>
    public static class DriverFactory
    {
        public static IWebDriver GetDriver() 
        {
            string driverSetting = SettingsProvider.GetSetting("Browser");
            IWebDriver driver;

            switch (driverSetting)
            {
                case "Chrome":
                    driver = new ChromeDriver();
                    break;

                case "Firefox":
                    driver = new FirefoxDriver();
                    break;

                case "Explorer":
                    driver = new InternetExplorerDriver();
                    break;

                default:
                    throw new Exception("Driver not found");
            }

            string environment = SettingsProvider.GetSetting("Environment");
            driver.Url = SettingsProvider.GetSetting("BaseUrl." + environment);

            return driver;
        }
    }
}
