﻿using System.Xml;

namespace API.Helpers
{
    public static class SettingsProvider
    {
        /// <summary>
        /// gets access to the values in the config file
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetSetting(string key)
        {
            string configurationFile = "Configuration.xml";
            string xpath = "/Configuration/Settings/";
            key = key.Replace('.', '/');
            xpath += key;
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(configurationFile);
            var nodes = xmlDoc.SelectNodes(xpath);
            return nodes[0].InnerText;
        }
    }
}
