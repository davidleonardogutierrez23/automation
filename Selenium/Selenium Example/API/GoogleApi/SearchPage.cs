﻿using OpenQA.Selenium;

namespace API.GoogleApi
{
    public class SearchPage : PageBase
    {
        //Web Elements
        private IWebElement searchInput;
        private IWebElement searchButton;
        private IWebElement luckyButton;

        private void InitFields() 
        {
            searchInput = driver.FindElement(By.Id("lst-ib"));
            searchButton = driver.FindElement(By.Name("btnK"));
            luckyButton = driver.FindElement(By.Name("btnI"));
        }

        /// <summary>
        /// Inizialites the Page
        /// </summary>
        private SearchPage()
        {
           InitFields();
        }

        /// <summary>
        /// Search a  term in the google search page
        /// </summary>
        /// <param name="searchTerm"></param>
        public void search(string searchTerm)
        {
            searchInput.SendKeys(searchTerm);
        }
        
        

        /// <summary>
        /// Returns the instance of the SearchPage
        /// </summary>
        /// <returns></returns>
        public static SearchPage GetSearchPage()
        {
            return new SearchPage();
        }
    }
}
