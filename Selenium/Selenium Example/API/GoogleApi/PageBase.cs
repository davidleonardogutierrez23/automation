﻿using API.Helpers;
using OpenQA.Selenium;

namespace API.GoogleApi
{
    public class PageBase
    {
        //Instance of the used driver
        public readonly IWebDriver driver = DriverFactory.GetDriver();
    }
}
